//``
/*
//Preguntar nombre al usuario
//Si es el mismo que el mio, imprimir "hola, yo tambien me llamo" + nombre
//Elejir otro nombre, si el nombre del usuario es el mismo, imprimir, "hola" + nombre "te llamas como..."
//Si no, solo imprimir "hola" + nombre

let nombre = prompt("Ingrese su nombre");

if (nombre === "Camila") {
    console.log(`Hola, yo tambien me llamo ${nombre}`);
} else if (nombre === "Sebastian") {
    console.log(`Hola ${nombre} te llamas como mi novio`);
} else {
    console.log(`Hola ${nombre}`)
}

//Preguntarle la edad al usuario
//Hacerle saber si tiene mas, menos, o la misma edad que nosotros

const MI_EDAD = 25; // En mayuscula porque es una constante que ya tiene un numero asignado.

const edadUsuario = prompt("¿Cual es tu edad?");

if (edadUsuario === MI_EDAD) {
    console.log(`Tenemos la misma edad!`);
} else if (edadUsuario > MI_EDAD) {
    console.log(`Sos mas grande que yo`);
} else if (edadUsuario < MI_EDAD) {
    console.log(`Sos mas chico que yo`);
} else {
    console.log(`No entendi la respuesta`);
}

//Preguntar si tiene documento, que conteste con si o no
//Si dice si, preguntar su edad
//Si es mayor de 18, puede entrar al bar
//Si no es mayor de 18, no puede entrar al bar
//Si no tiene documento, no puede entrar al bar

let documentoUsuario = prompt(`Tenes documento?`, "'si' o 'no'").toLowerCase();
let edad;

if (documentoUsuario === "si") {
    edad = Number(prompt("Cuantos años tenes?"));
    if (edad >= 18) {
        prompt("Podes entrar al bar!");
    } else if (edad < 18) {
        prompt("No podes ingresar al bar. Se necesita documento")
    }
} else {
    prompt("No podes ingresar al bar")
}

//Clase 4:
//Usar un bucle While para registrar los numeros del 1 al 10

let numero = 0;

while (numero < 10) {
    numero += 1;
}

//Usar un bucle While para registrar los numeros del 10 al 1:

let nuevoNumero = 10;

while (numero > 10) {
    numero -= 1;
}

//Imprimir cada 3er numero del 3 al 22 usando bucle for:

function parte1() {
    for (let i = 3; i <= 22; i++) {
        console.log(i);
    }
}
parte1();

//Usando un bucle while, decirle a la computadora que registre los numeros de 10 a 1:


function tarea2() {
    let i = 11;
    while (i > 0) {
        i--; // i-= 1; i = i -1
        console.log(i);
    }
}
tarea2();

// Ejercicio con arrays

let animalesFavoritos = ['gatos', 'perros', 'conejos', 'caballos', 'ardillas']

console.log(animalesFavoritos)
console.log(animalesFavoritos.length);
console.log(animalesFavoritos[3]);

animalesFavoritos[1] = 'peces';
animalesFavoritos.push('iguanas')
console.log(animalesFavoritos)

//Iterar sobre cada uno, usando FOR

for (i = 0; i < animalesFavoritos.length; i++) {
    console.log(`me gustan les ${animalesFavoritos[i]}`)
}

//Contar de 10 a 0 y cuando llegue a 5 mostrar un msj

for (let i = 10; i >= 0; i--) {
    if (i === 5) {
        console.log('estamos en el medio');
    } else {
        console.log(i);
    }
}
*/
// Calcular el promedio de todos los números en un array de números. (y ponerlo en una función)
// ej.: calcularPromedio([10,5,4,2,8])

const notasParcial = [10, 5, 4, 2, 8]
let totalNotas = 0;

for (let i = 0; i < notasParcial.length; i++) {
    totalNotas = totalNotas + notasParcial[i];
}

//console.log(totalNotas);
//console.log('El promedio es ' + totalNotas / notasParcial.length);

//Hacer una funcion llamada calcularPromedio que tome como parametro un array

/*


/*
//Ejercicio con array

const comidas = ['pizza', 'fideos', 'milanesa'];
console.log(comidas);

comidas[1] = 'nose'
console.log(comidas)
comidas.push('papas')
console.log(comidas)


//'FizzBuzz'
//Contar del 1 al 50 e imprimir los numeros;
//Si un numero es multiplo de tres, imprimir 'Fizz'
//Si un numero es multiplo de 5, imprimir 'Buzz'
//Si es multiplo de 3 y 5 imprimir 'FizzBuzz'

function fizzBuzz() {
    for (let i = 1; i <= 50; i++) {
        if (i % 3 && i % 5 === 0) {
            console.log('fizzbuzz')
        } else if (i % 3 === 0) {
            console.log('fizz')
        } else if (i % 5 === 0) {
            console.log('buzz')
        } else {
            console.log(i)
        }
    }
}
fizzBuzz();
//

function fizzBuzzClasico() {
    for (let i = 1; i <= 50; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            console.log('FizzBuzz');
        } else if (i % 3 === 0) {
            console.log('Fizz');
        } else if (i % 5 === 0) {
            console.log('Buzz');
        } else {
            console.log(i);
        }
    }
}
fizzBuzzClasico();

//Otro ejercicio con operador ternario:

function verificarEdadEntrada(edad = 18) {
    edad >= 18 ? console.log('Bienvenido al bar') : console.log('Sos menor de edad');
    // (condicion) ? izq : der; 
}

TAREAS
// Preguntar el salario anual y calcular el salario mensual
// Preguntar el salario mensual y calcular el anual
// diario... semanal, por hora. etc.


//DOM

//let x = document.createElement('div'); // crea un nuevo elemento 
//document.createTextNode('aaa'); // crea un texto nuevo
//existingNode(ej:footer).appendChild(newNode/ ej: x); // agrega un nuevo nodo al final de existingNode.

const nodoPagina = document.querySelector('body');
const nuevoParrafo = document.createElement('p');
const textoParrafo = document.createTextNode('aaa');
nuevoParrafo.appendChild(textoParrafo);
nodoPagina.appendChild(nuevoParrafo);


//Clase5:

//crear un formulario donde un usuario pueda ingresar su salario anual.
//cuando el usuario haga click en el botón "calcular", mostrar el salario mensual
//en una caja de texto deshabilitada. --> <input type="text" disabled id="salario-mensual"/>
*/

function calcularSalarioMensual(salarioAnual) {
    return salarioAnual / 12;
}

// $calcularSalarioMensual tiene $ porque es un elemento, no es la funcion

const $calcularSalarioMensual = document.querySelector('#calcular-salario-mensual');

// cuando le haga CLICK a este elemento, ejecuta esta funcion

$calcularSalarioMensual.onclick = function () {
    const salarioAnual = document.querySelector('#salario-anual').value;

    console.log('click');
    console.log(salarioAnual);
    return false; //el form no se envia.
}


//creá un formulario que capture el primer nombre, segundo nombre, apellido/s y edad del usuario
//también vamos a crear un <h1> que diga Bienvenido!
//vas a crear un botón de acción que una vez que lo apretás, va a
//mostrar toda la información junta en un campo de texto
//Y va a cambiar el <h1> para decir "Bienvenido, nombreDeUsuario"!


const $mostrarInformacionPersonal = document.querySelector('#mostrar-informacion');

$mostrarInformacionPersonal.onclick = function () {
    const nombreUsuario = document.querySelector('#nombre-usuario').value
    const segundoNombre = document.querySelector('#segundo-nombre').value;
    const apellidoUsuario = document.querySelector('#apellido-usuario').value;
    const edadUsuario = document.querySelector('#edad-usuario').value;

    const $nodoPagina = document.querySelector('body');
    const $parrafoDeInfoPersonal = document.createElement('p');
    const $textoInformacionUsuario = document.createTextNode(`tu nombre completo es ${nombreUsuario} ${segundoNombre} ${apellidoUsuario} y tu edad es ${edadUsuario}`)
    $parrafoDeInfoPersonal.appendChild($textoInformacionUsuario);
    $nodoPagina.appendChild($parrafoDeInfoPersonal)

    const $tituloNombre = document.querySelector('h1');
    $tituloNombre.textContent = `Hola ${nombreUsuario}`
    return false;
}
/*
    //CLASE 6:

    //Empezar preguntando cuánta gente hay en el grupo familiar.
    //Crear tantos inputs+labels como gente haya para completar la edad de cada integrante.
    //Al hacer click en "calcular", mostrar en un elemento pre-existente la mayor edad, la menor edad y el promedio del grupo familiar.
    //Punto bonus: Crear un botón para "empezar de nuevo" que empiece el proceso nuevamente, borrando los inputs ya creados (investigar cómo en MDN).
*/

// const $agregarCantidadIntegrantes = document.querySelector('#agregar-integrantes');

// $agregarCantidadIntegrantes.onclick = function (event) {
//     const $cantidadIntegrantes = document.querySelector('#cantidad-integrantes');
//     const cantidadIntegrantes = Number($cantidadIntegrantes.value)

//     crearIntegrantes()

//     event.preventDefault();

// }

// function crearIntegrantes() {
//     for (let i = 0; i < cantidadIntegrantes; i++) {
//         crearIntegrante(i);
//     }
// }

// function crearIntegrante(indice) {
//     const $div = document.createElement('div')
//     $div.className = 'integrante';

//     const $label = document.createElement('label');
//     $label.textContent = 'La edad del integrante # ' + (indice + 1)
//     const $input = document.createElement('input')
//     $input.type = 'number';

//     $div.appendChild($label)
//     $div.appendChild($input)

// }


/*
    //Crear una interfaz que permita agregar ó quitar (botones agregar y quitar) inputs+labels para completar el salario anual de cada integrante de la familia que trabaje.
    //Al hacer click en "calcular", mostrar en un elemento pre-existente el mayor salario anual, menor salario anual, salario anual promedio y salario mensual promedio.

    //Punto bonus: si hay inputs vacíos, ignorarlos en el cálculo (no contarlos como 0).

*/
