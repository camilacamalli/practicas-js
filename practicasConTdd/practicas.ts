/*
Desafío de programación #12: Crea una función que reciba un array de números y devuelva un array conteniendo solo los números positivos
*/
type FuncMostrarNumerosPositivos = (
    arrayDeNumeros:number []
) => string | number[];

const arrayPositivo = [];

const mostrarNumerosPositivos: FuncMostrarNumerosPositivos = (arrayDeNumeros:number []) =>{
    for(let i = 0; i < arrayDeNumeros.length; i++){
        if(arrayDeNumeros[i] >= 0)
        arrayPositivo.push(arrayDeNumeros[i])
    }
    return arrayPositivo
}


export default mostrarNumerosPositivos;