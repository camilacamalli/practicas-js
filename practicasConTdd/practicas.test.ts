import mostrarNumerosPositivos from "./practicas"
/*
const suma = require('./script.js');
const devolverNumerosPositivos = require('./script.js')

test('sumar 1 + 2 es igual a 3', () => {
    expect(suma(1, 2)).toBe(3);
});
*/

test('A partir de un array de numeros, devuelve solo los positivos', () => {
    expect(mostrarNumerosPositivos([1, 5, 7, -2, 8, -12, 15])).toStrictEqual([1, 5, 7, 8, 15])
})
